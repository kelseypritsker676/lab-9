/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author kelsey.pritsker676
 */
public class DonationManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {//database stuff:
            String host = "jdbc:derby://localhost:1527/CamaStateDonationSystem";
            String dName = "donate";
            String dPass = "donate";
            Connection conn = DriverManager.getConnection(host, dName, dPass);
            Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet Rset;
            String SQL;

            Scanner scan = new Scanner(System.in);
            DonationModel model = new DonationModel();
            DonationView view = new DonationView();

            DonationController controller = new DonationController(view, model);
            int input = 0;
            do {
                System.out.println("Please enter number of desired action: "
                        + "\n0: End donation manager"
                        + "\n1: Donate"
                        + "\n2: Find amount donated by individual"
                        + "\n3: List bronze doners"
                        + "\n4: List silver doners"
                        + "\n5: List gold doners"
                        + "\n6: Print database");
                input = scan.nextInt();
                switch (input) {
                    case 0://end donation manager
                        System.out.println("Donatation manager is ending.");
                        System.exit(0);
                        break;
                    case 1: //start donation GUI
                        view.setVisible(true);
                        break;
                    case 2://Find amount donated by person
                        System.out.println("Please enter full name");
                        String name1 = scan.next();
                        String name2 = scan.next();
                        String name = name1 + " " + name2;
                        SQL = "SELECT * FROM APP.DONATIONDATABASE\n"
                                + "WHERE FullName='" + name + "'";
                        Rset = state.executeQuery(SQL);
                        while(Rset.next()){
                            System.out.println("\n" + name + " donated " + Rset.getString("DONATIONAMOUNT") + "\n");
                        }
                        break;
                    case 3: //List Bronze Doners
                        SQL = "SELECT * FROM APP.DONATIONDATABASE\n"
                                + "WHERE Prize='BRONZE'";
                        Rset = state.executeQuery(SQL);
                        while(Rset.next()){
                            System.out.println("\nBronze Doners\n"
                                    + Rset.getString("FULLNAME") + " who donated " + Rset.getString("DONATIONAMOUNT") + "\n");
                        }
                        Rset = state.executeQuery(SQL);
                        break;
                    case 4: //List Silver Doners
                        SQL = "SELECT * FROM APP.DONATIONDATABASE\n"
                                + "WHERE Prize='SILVER'";
                        Rset = state.executeQuery(SQL);
                        while(Rset.next()){
                            System.out.println("\nSilver Doners\n"
                                    + Rset.getString("FULLNAME") + " who donated " + Rset.getString("DONATIONAMOUNT") + "\n");
                        }
                        Rset = state.executeQuery(SQL);
                        break;
                    case 5: //List Gold Doners
                        SQL = "SELECT * FROM APP.DONATIONDATABASE\n"
                                + "WHERE Prize='GOLD'";
                        Rset = state.executeQuery(SQL);
                        while(Rset.next()){
                            System.out.println("\nGold Doners\n"
                                    + Rset.getString("FULLNAME") + " who donated " + Rset.getString("DONATIONAMOUNT") + "\n");
                        }
                        Rset = state.executeQuery(SQL);
                        break;
                    case 6: //Print database
                        SQL = "SELECT * FROM APP.DONATIONDATABASE";
                        Rset = state.executeQuery(SQL);
                        System.out.println("\nFULL NAME" + "\t" + "EMAIL" + "\t\t\t" + "DONATION");
                        while (Rset.next()) {;
                            String sFullName = Rset.getString("FULLNAME");
                            String sEMail = Rset.getString("EMAIL");
                            double sDonation = Rset.getDouble("DONATIONAMOUNT");


                            System.out.println(sFullName + "\t" + sEMail + "\t\t" + sDonation);
                        }
                        System.out.println("\n");
                }
            } while (input != 0);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author joel.helling904
 */
public class DonationController {

    private DonationModel model;
    private DonationView view;

    public DonationController(DonationView view, DonationModel model) {
        this.view = view;
        this.model = model;

        view.addListenerSubmit(new ListenerSubmit());
        view.addListenerCancel(new CancelSubmit());
    }

    private class CancelSubmit implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                view.setVisible(false);
                System.exit(0);
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }

    private class ListenerSubmit implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                try {
                    String host = "jdbc:derby://localhost:1527/CamaStateDonationSystem";
                    String dName = "donate";
                    String dPass = "donate";
                    Connection conn = DriverManager.getConnection(host, dName, dPass);
                    Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    

                    String name = view.getName();

                    String email = view.getEmail();
                    int donation = view.getDonation();
                    String prize = model.calculatePlague(donation);
                    String SQL = "INSERT INTO APP.DONATIONDATABASE (FullName, eMail, DonationAmount, Prize)\n"
                            + "VALUES ('" + name + "','" + email + "'," + donation + ",'" + prize + "')";
                    int Rset = state.executeUpdate(SQL);
                    //model.calculatePlague(donation));
                    view.setVisible(false);
                    view.dispose();
                    boolean sentEmail = model.sendEmail(name, email,model.calculatePlague(donation));
                    //System.out.printf("%s\n", sentEmail);
                } catch (SQLException err) {
                    System.out.println(err.getMessage());
                }
            } catch (NumberFormatException nfex) {
                //view.showError("Bad input: '" + amount + "'");
            }
        }
    }
}

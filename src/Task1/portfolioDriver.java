/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Task1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


public class portfolioDriver {
 
    public static void main(String[] args) {
    try{
        String host = "jdbc:derby://localhost:1527/BigInvest";
        String dName = "lab9";
        String dPass= "lab9";
        Connection conn = DriverManager.getConnection( host, dName, dPass );
        Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String SQL = "SELECT * FROM APP.PORTFOLIO";
        ResultSet Rset = state.executeQuery( SQL );
       
        System.out.println("ID"+"\t"+"SYMBOL"+"\t"+"QUANTITY"+"\t"+"PRICE"+"\t"+"TOTAL");
        while(Rset.next( )){;
        int Id = Rset.getInt("ID");
        String sSymbol = Rset.getString("SYMBOL");
        double sQuantity = Rset.getDouble("QUANTITY");
        double sPrice = Rset.getDouble("PRICE");
        double sTotal = sQuantity*sPrice;
        
       
        System.out.println(Id+"\t"+sSymbol+"\t"+sQuantity+"\t\t"+sPrice+"\t"+sTotal);
       }
    }
   catch ( SQLException err ) {
        System.out.println( err.getMessage( ) );
    }
  }
}